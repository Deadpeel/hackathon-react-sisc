const Sequelize = require("sequelize")

const database = new Sequelize("TicketApp", 'root', '', {
    host: 'localhost',
    dialect: 'mysql',
    pool: {
    max: 15,
    min: 5,
    idle: 200000,
    evict: 150000,
    acquire: 300000
  },
})

module.exports = database;