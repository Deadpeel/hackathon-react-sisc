const users = require("./users");
const spectacole = require("./spectacole");

const controllers = {
    users,
    spectacole,
};

module.exports = controllers;