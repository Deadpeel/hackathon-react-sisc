const UsersDB = require("../models").users;

const controller = {
    login: async (req,res) =>{
        console.log(req.headers)
        const user = await UsersDB.findOne({ where: { email: req.headers.email } });
        console.log(user)
        if(user !== null)
            res.status(200).send(user);
        else
            res.status(401).send({"message":"Contul nu exista"})
    },

    register: async (req,res) =>{
        const user = {
            name: req.body.name,
            email: req.body.email,
            password: req.body.password,
        };

        try {
            await UsersDB.create(user)
            res.status(201).send(user)
        } catch (err) {
            res.status(500).send(err)
        }
    }

};

module.exports = controller;
