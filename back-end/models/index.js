const Sequelize = require("sequelize")
const database = require("../config/database");
const UsersModel = require("./users")
const LocuriModel = require("./locuri")
const RezervariModel = require("./rezervari")
const SaliModel = require("./sali")
const SpectacoleModel = require("./spectacole")

const users = UsersModel(database,Sequelize);
const locuri = LocuriModel(database,Sequelize);
const rezervari = RezervariModel(database,Sequelize);
const sali = SaliModel(database,Sequelize);
const spectacole = SpectacoleModel(database,Sequelize);

spectacole.hasOne(sali, {foreignKey: "spectacolId"})
locuri.hasOne(rezervari, {foreignKey: "locId"})
sali.hasMany(locuri, {foreignKey: "salaId"})

users.belongsToMany(spectacole, {through: rezervari})

module.exports = {
    users,
    locuri,
    rezervari,
    sali,
    spectacole,
    connection: database,
};