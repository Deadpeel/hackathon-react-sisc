module.exports = (sequelize, DataTypes) => {
    return sequelize.define(
        "locuri",
        {
            rand: DataTypes.INTEGER,
            loc: DataTypes.INTEGER,
        },
        {
            underscored: true,
            freezeTableName: true
        }
    );
};