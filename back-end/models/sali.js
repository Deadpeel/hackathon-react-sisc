module.exports = (sequelize, DataTypes) => {
    return sequelize.define(
        "sali",
        {
            name: DataTypes.STRING,
            maxSeats: DataTypes.INTEGER,
        },
        {
            underscored: true,
            freezeTableName: true
        }
    );
};