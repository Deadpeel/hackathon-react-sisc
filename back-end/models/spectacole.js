module.exports = (sequelize, DataTypes) => {
    return sequelize.define(
        "spectacole",
        {
            name: DataTypes.STRING,
            genuri: DataTypes.STRING,
            description: DataTypes.STRING,
        },
        {
            underscored: true,
            freezeTableName: true
        }
    );
};