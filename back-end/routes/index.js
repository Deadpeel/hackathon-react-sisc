const express = require('express');
const router = express.Router();

const usersRouter = require('./users');
const spectacoleRouter = require('./spectacole');

router.use('/users', usersRouter);
router.use('/spectacole', spectacoleRouter);


module.exports = router;