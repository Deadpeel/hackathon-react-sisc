const express = require("express");
const router = express.Router();
const spectacoleController = require("../controllers").spectacole;

router.get("/getAllSpectacole", spectacoleController.getAllSpectacole);

module.exports = router;