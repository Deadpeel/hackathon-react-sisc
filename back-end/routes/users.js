const express = require("express");
const router = express.Router();
const usersController = require("../controllers").users;

router.post("/register", usersController.register);
router.get("/login", usersController.login);

module.exports = router;