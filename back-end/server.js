const express = require("express");
const cors = require("cors");
const router = require("./routes");
const db = require("./models/index").connection;
const LocuriDB = require("./models").locuri;
const SaliDB = require("./models").sali;

const app = express();
const PORT = 1234;

app.use(express.json());
app.use(express.urlencoded({ extended: true}));

app.use(
    cors({origin: "*",
})
)


app.listen(PORT, () => {
    console.log(`localhost:${PORT}`);
});

app.use("/api", router);


app.get("/reset", async (req, res) => {
	try {
        await db.sync({ force: true});
        for(let i = 1; i<=10;i++){

            let noSeats = Math.floor(Math.random() * (200 - 50 + 1)) + 50;
            await SaliDB.create({
                name:`Sala ${i}`, 
                maxSeats: noSeats
            });

            const rows = noSeats/10;
            const seats = noSeats/rows;
            const leftSeats = noSeats - Math.trunc(rows)*seats;

            for(let j = 1; j<=Math.trunc(rows);j++){
                for(let k = 1; k<=seats;k++){
                    await LocuriDB.create({rand:j,loc:k, salaId:i});
                }
            }

            for(let j = 1; j<=leftSeats;j++){
                console.log(Math.trunc(rows)+1, j, i)
                await LocuriDB.create({rand:Math.trunc(rows)+1,loc:j, salaId:i});
            }

        }
		res.status(200).send("Reset complete!");
	} catch (error) {
		console.log(error.message);
		res.status(500).send("Reset failed!");
	}
});



