import { useState } from 'react'
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Login from './Pages/Login/Login'
import './App.css'
import SideBar from "./Components/SideBar/SideBar";
import Home from "./Pages/Home/Home";
import Test from "./Pages/test";
import Locuri from './Pages/Locuri/Locuri'

function App() {
return (
  <Router>
    <div className='container-router'>
        <Routes>
          <Route index element={<Home />}/>
          <Route path="/login" element={<Login />} />
          <Route path="/test/:id" element={<Locuri /> } />
        </Routes>
    </div>
  </Router>
);
}

export default App
