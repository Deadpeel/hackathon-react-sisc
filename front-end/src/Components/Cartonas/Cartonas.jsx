import { useNavigate } from 'react-router-dom';
import './Cartonas.css';
const Cartonas = ({titlu, descriere, poza,id}) => {
    const navigate = useNavigate()
    return (
            <div className="cartonas" onClick={()=>{navigate(`test/${id}`)}}>
                <div class="poza"><img class="imagine" src={poza}/></div>
                <div className="text-box">
                    <h3>{titlu}</h3>
                    <p>{descriere}</p>
                </div>
            </div>
          

    )
}

export default Cartonas;