 import './Home.css'
 import Cartonas from '../../Components/Cartonas/Cartonas'
 import React, { useState,useEffect } from 'react'
 import Select from 'react-select'
import imagine1 from '../../assets/poze-home/poza_5.jpg'
import imagine2 from '../../assets/poze-home/poza_6.jpg'
import imagine3 from '../../assets/poze-home/poza_10.jpg'
import imagine4 from '../..//assets/poze-home/poza_7.jpeg'
import imagine5 from '../../assets/poze-home/poza_2.jpeg'
import imagine6 from '../../assets/poze-home/poza_3.jpeg'
import imagine7 from '../../assets/poze-home/poza_4.jpeg'
import imagine8 from '../../assets/poze-home/poza_1.jpeg'
import imagine9 from '../../assets/poze-home/poza_8.jpeg'
import imagine10 from '../../assets/poze-home/poza_9.jpeg'
import { useNavigate } from 'react-router-dom'

const Home = () => {

    const navigate = useNavigate();

    useEffect(()=>{
        // axios.get("http://localhost:1234/api/spectacole/getAllSpectacole")
        // .then(()=>{

        // })
    },[])

    const options = [
    { value: 'chocolate', label: 'Chocolate'},
    { value: 'strawberry', label: 'Strawberry'},
    { value: 'vanilla', label: 'Vanilla'}
    ]

    const [selectedOption, setSelectedOption] = useState(options);

    const spectacole = [
        {id:1, titlu: 'chocolate', descriere: 'Chocolate', pozaCartonas: imagine1},
        {id:2, titlu: 'strawberry', descriere: 'Strawberry', pozaCartonas: imagine2},
        {id:3, titlu: 'vanilla', descriere: 'Vanilla', pozaCartonas: imagine3},
        {id:4, titlu: 'vanilla', descriere: 'Vanilla', pozaCartonas: imagine4},
        {id:5, titlu: 'vanilla', descriere: 'Vanilla', pozaCartonas: imagine5},
        {id:6, titlu: 'vanilla', descriere: 'Vanilla', pozaCartonas: imagine6},
        {id:7, titlu: 'vanilla', descriere: 'Vanilla', pozaCartonas: imagine7},
        {id:8, titlu: 'vanilla', descriere: 'Vanilla', pozaCartonas: imagine8},
        {id:9, titlu: 'vanilla', descriere: 'Vanilla', pozaCartonas: imagine9},
        {id:0, titlu: 'vanilla', descriere: 'Vanilla', pozaCartonas: imagine10}

        ]


    return (
        <div className="container-body">
            
            <button className='buttton' id='buton-login' onClick={()=>{navigate('/login')}}>Login</button>
            
            <div className="search-bar">
                <Select 
                options={options} 
                 styles={{
                     control: (baseStyles, SearchBar) => ({
                     ...baseStyles,
                     fontSize: 18,
                     width: '600px',
                     height: '20px'
                     })
                 }} 
                />
            </div>

           <div class="container-cartonase">
            {spectacole.map((spectacol) => {
                const handleFilter = (event) => {
                    const value = event.target.value;
                    const filtered = selectedOption.options(option => option.titlu.includes(value));
                    setFilteredOptions(filtered);
                  };
                return (
                <Cartonas onChange={handleFilter}
                titlu={spectacol.titlu} 
                descriere={spectacol.descriere}
                poza={spectacol.pozaCartonas}
                id={spectacol.id}/>
                // console.log(spectacol.titlu);
                )
            })
            }
            </div>

        </div>
    
    )

}
export default Home;
