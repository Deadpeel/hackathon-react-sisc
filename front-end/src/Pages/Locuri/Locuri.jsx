import { useEffect, useState } from 'react';
import './Locuri.css';

function Locuri() {
    const data = Array.from({ length: 100 }, (_, index) => ({ index, state: 0 }));
    const [locuriSelectate, setLocuriSelectate] = useState([]);

    const pushare = (item) => {
        setLocuriSelectate([...locuriSelectate, item]);
    };

    const deleteItem = (index) => {
        const updatedLocuriSelectate = locuriSelectate.filter((item, i) => i !== index);
        setLocuriSelectate(updatedLocuriSelectate);
    };

    return (
        <>
        <div className='pagina'>
            <div className='TV'>
                <div className="cinema">Caca</div>
            <p>Pup jos</p>
            </div>
            <div className="tot">
                <div className="sala">
                    {data.map((loc) => (
                        <div
                            key={loc.index}
                            className={`state-zero ${locuriSelectate.find((item) => item.index === loc.index) ? 'selected' : ''}`}
                            onClick={() => pushare(loc)}
                        >
                            <span className='cute'>{loc.index + 1}</span>
                            <span>{loc.state}</span>
                            
                        </div>
                    ))}
                </div>
                <div className="subtotal">
                <button className="submitSelection">Confirma!</button>
                    {locuriSelectate.map((item, index) => (
                        <div key={index} className="itemTotal">
                            
                            <span className='caca'>Locul {item.index + 1} a fost adaugat!</span>
                            
                            <button onClick={() => deleteItem(index)}>Sterge</button>
                        </div>
                    ))}
                    
                </div>
            </div>
        </div>
        </>
    );
}

export default Locuri;
