import { useState } from 'react';
import { useNavigate } from "react-router-dom";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Login = () => {
    const navigate = useNavigate();

    const [login, setLogin] = useState(false);
    
    const [data, setData] =useState({
        nume: '',
        email: '',
        varsta: '',
        parola: '',
        confirmareParola: ''
    });

    const inputData = (field, value) => {
        setData(prev => ({
            ...prev,
            [field]: value
        }));
    };

    const handleRegister = () => {
        if(data.confirmareParola===data.parola && login===false){
            console.log(data);
        }else {toast("Parolele nu coincid!")}
    };

    const handleLogin = () => {
        console.log(data);
        navigate("../");
    }


    return(
        <div className='container-Login'>
           <div className="register-container">

                {/* <button onClick={() => {setLogin((login) => !login)}} >Test</button> */}

                {!login ?
                    <div className='register-form'>
                    <label htmlFor="">Nume</label>
                    <input type="text" name="Nume" value={data.nume} onChange={(e) => inputData('nume', e.target.value)}  id="" placeholder='Nume'/>

                    <label htmlFor="">Email</label>
                    <input type="email" name="Email" value={data.email} onChange={(e) => inputData('email', e.target.value)} id="" placeholder='Email' />

                    <label htmlFor="">Varsta</label>
                    <input type="number" name="Varsta" value={data.varsta} onChange={(e) => inputData('varsta', e.target.value)} id="" placeholder='Varsta'/>

                    <label htmlFor="">Parola</label>
                    <input type="password" name="Parola" value={data.parola} onChange={(e) => inputData('parola', e.target.value)} id="" placeholder='Parola' />

                    <label htmlFor="">Confirmare parola</label>
                    <input type="password" name="ConfirmareParola" value={data.confirmareParola} onChange={(e) => inputData('confirmareParola', e.target.value)} id="" placeholder='Parola'/>
                    
                    <button className='button' onClick={handleRegister}>Register</button>

                    <div className='scris-button' onClick={() => {setLogin((login) => !login)}}>Ai deja cont?</div> 
                    </div>
                : <div className='register-form'>
                    <label htmlFor="">Email</label>
                    <input type="email" name="Email" value={data.nume} onChange={(e) => inputData('nume', e.target.value)} id="" placeholder='Email' />

                    <label htmlFor="">Parola</label>
                    <input type="password" name="Parola" value={data.parola} onChange={(e) => inputData('parola', e.target.value)} id="" placeholder='Parola' />

                    <button className='button' onClick={handleLogin}>Login</button>

                    <div className='scris-button' onClick={() => {setLogin((login) => !login)}}>Nu ai cont?</div>
                    </div>}

                    {/* { data.parola==data.confirmareParola? console.log(data) : (<div className='atentionare'>Parolele nu coincid!</div>)} */}
                    

            </div>
            <ToastContainer />
        </div>
    )
}

export default Login;